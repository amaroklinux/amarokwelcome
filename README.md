# The Amarok Linux Welcome screen

 Shows important information about the release/edition of Amarok Linux.

![Screenshot of Amarok 23.2](/screencaptures/welcome_screen_amarok_23_2.png?raw=true)
